<?php
namespace Modules\Order\Http\Controllers;

use Modules\Order\Http\Requests\VehiculoRequest;
use Modules\Order\Http\Resources\VehiculoCollection;
use Modules\Order\Http\Resources\VehiculoResource;
use App\Models\Tenant\Catalogs\IdentityDocumentType;
use App\Http\Controllers\Controller;
use Modules\Order\Models\Vehiculo;
use Illuminate\Http\Request;

class VehiculoController extends Controller
{

    public function index()
    {
        return view('order::vehiculos.index');
    }

    public function columns()
    {
        return [
            'placa' => 'placa',
            'modelo' => 'modelo',
        ];
    }

    public function records(Request $request)
    {

        $records = Vehiculo::where($request->column, 'like', "%{$request->value}%")
                            ->orderBy('placa');

        return new VehiculoCollection($records->paginate(config('tenant.items_per_page')));
    }


    public function tables()
    {
        $identity_document_types = IdentityDocumentType::whereActive()->get();
        $api_service_token = config('configuration.api_service_token');

        return compact('identity_document_types', 'api_service_token');
    }

    public function record($id)
    {
        $record = new VehiculoResource(Vehiculo::findOrFail($id));

        return $record;
    }

    public function store(VehiculoRequest $request)
    {

        $id = $request->input('id');
        $record = Vehiculo::firstOrNew(['id' => $id]);
        $record->fill($request->all());
        $record->save();

        return [
            'success' => true,
            'message' => ($id)?'Vehiculo editado con éxito':'Vehiculo registrado con éxito',
            'id' => $record->id
        ];
    }

    public function destroy($id)
    {

        $record = Vehiculo::findOrFail($id);
        $record->delete();

        return [
            'success' => true,
            'message' => 'Vehiculo eliminado con éxito'
        ];

    }

}
