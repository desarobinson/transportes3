<?php

namespace Modules\Inventory\Http\Controllers;

use App\Http\Controllers\Controller;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use App\Models\Tenant\Establishment;
use App\Models\Tenant\Company;
use App\Models\Tenant\Item;
use Modules\Inventory\Models\ItemWarehouse;
use Modules\Inventory\Exports\ExpenseExport;
use Modules\Inventory\Models\Warehouse;
use Modules\Expense\Models\Expense;
use Modules\Order\Models\Vehiculo;

use Carbon\Carbon;

class ReportExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {


        if($request->warehouse_id && $request->warehouse_id != 'all')
        {
            $reports = Expense::select('id', 'placa','date_of_issue','supplier','total')
            ->where('placa','=',$request->warehouse_id)
            ->latest()->paginate(config('tenant.items_per_page'));
           
        }
        else{
            $reports = Expense::select('id', 'placa','date_of_issue','supplier','total')
           
            ->latest()->paginate(config('tenant.items_per_page'));
        }

        
        $vehiculos = Vehiculo::select('id', 'placa','marca')->get();
        $warehouses = Warehouse::select('id', 'description')->get();

        return view('inventory::reports.expense.index', compact('reports', 'warehouses','vehiculos'));
    }

    /**
     * Search
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request) {

        $reports = Expense::select('id', 'placa','date_of_issue','supplier','total')
            ->where('placa','=',$request->warehouse_id)
            ->latest()->paginate(config('tenant.items_per_page'));

        return view('inventory::expense.inventory.index', compact('reports'));
    }

    /**
     * PDF
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function pdf(Request $request) {

        $company = Company::first();
        $establishment = Establishment::first();
        ini_set('max_execution_time', 0);

        if($request->warehouse_id && $request->warehouse_id != 'all')
        {
            $reports = Expense::select('id', 'placa','date_of_issue','supplier','total')
            ->where('placa','=',$request->warehouse_id)
            ->latest()->get();
        }
        else{

            $reports = Expense::select('id', 'placa','date_of_issue','supplier','total')
            ->latest()->get();
        }



        $pdf = PDF::loadView('inventory::reports.expense.report_pdf', compact("reports", "company", "establishment"));
        $filename = 'Reporte_Inventario'.date('YmdHis');

        return $pdf->download($filename.'.pdf');
    }

    /**
     * Excel
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function excel(Request $request) {
        $company = Company::first();
        $establishment = Establishment::first();


        if($request->warehouse_id && $request->warehouse_id != 'all')
        {
            $records = Expense::select('id', 'placa','date_of_issue','supplier','total')
            ->where('placa','=',$request->warehouse_id)
            ->latest()->get();

        }
        else{
            $records = Expense::select('id', 'placa','date_of_issue','supplier','total')
            ->latest()->get();

        }


        return (new ExpenseExport)
            ->records($records)
            ->company($company)
            ->establishment($establishment)
            ->download('Reportegasto'.Carbon::now().'.xlsx');
    }
}
